# Event Sourcing

* [Installation](./docs/installation.md)

* [Usage](./docs/usage.md)

* [Persistence](./docs/persistence.md)

* [Snapshot](./docs/snapshot.md)

* [Upcast](./docs/upcast.md)



## Install dependencies
```
bin/composer.sh install
```

## Execute tests
```
bin/test.sh
bin/test-coverage.sh
bin/test-xdebug.sh
```

## Execute code tools
```
bin/phpstan.sh
bin/rector.sh
```