<?php

declare(strict_types=1);

namespace Zisato\EventSourcing\Aggregate\Exception;

final class AggregateReconstituteException extends \RuntimeException
{
}
