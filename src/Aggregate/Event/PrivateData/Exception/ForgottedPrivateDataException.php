<?php

declare(strict_types=1);

namespace Zisato\EventSourcing\Aggregate\Event\PrivateData\Exception;

class ForgottedPrivateDataException extends \Exception
{
}
